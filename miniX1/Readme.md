[**Click here to see my project**](https://julieanh.gitlab.io/aesthetic-programming/miniX1/) <-----

# My first miniX

As described by Winnie Soon & Geoff Cox, one of the first things you learn as a beginner programmer is print(“hello world”). As my first programming experience with JavaScript, I have made a sort of visual version of such. It is an interactive drawing of sorts. Like one of the many drawing you did as a kid of a house, grass, blue sky and a sun in the corner. I made many of those and they became part of my basis of what I could do with pen and paper. But I wanted it to be something more than an image on my screen, to be interactive in some way. So, following its childish connotations, I hid the “drawing”. In order, to make the drawing appear, you must interact physically with the computer and press any key. When the key is released the drawing disappear. You are in control of for how long or if you want to show the secret drawing.
<br>

<img src ='firstglance.png' width=400> <img src ='drawing.png' width=400>

I have set my canvas to a 1050x800 and framerate of 17. I chose the framerate according to how flashy I wanted the colors in the window should fluctuate. I have made use of different geometric shapes such as rectangle, circle, and triangle to make up the different components in the “drawing” like the house, roof, and sun. I have specified the color of these both when key is pressed and when no key is pressed. When no key is pressed, the objects appear as the same color as the set background color, making them “invisible”. I have removed any outline of the shapes, to make them completely vanish. 
I have learned an array of different syntaxes and the importance of writing notes to myself within my code using //. It helped me keep track of my code when I had multiple similar shapes repeating on different coordinates on my canvas. 
<br>

While working on my own code, I sparred with my buddy group on different references on p5*.js. Which they had incorporated in their code and had them explain how it worked. After having spent some time working by myself on my own code, I found it easier to understand and read others code and have them explain it while doing so. It gave me inspiration for my own coding and helped me identify errors.
<br>

Writing has a structure to it, the same way coding does. Both in the direction you read from top to bottom and left to right. When writing a story, you need to write in a certain structure that make the text make sense to the reader, otherwise it can fail in trying to convey its message/story. It’s the same within coding, while it is a bit more unforgiving. You need to write in a way that make sense to a computer which interpretation skills are more limited. Spelling errors aren’t welcome in most types of writing nor in coding, but the consequences of such differ significantly. You can still (depending on the severity of spelling errors) read through a text but the code won’t run.  
The ability to write and read invites you into the meaning behind the symbols on the page. You can re-write the story or create your own. Writing can give you the ability to be heard by and reach more people. I find that coding has many of the same attributes. If you cannot code, you are not able to re-write it or write your own meaning. You will be excluded from the power and liberty it can provide. As Annette Vee points out, coding is frequently being compared to literacy and how literacy in the future may be understood to include the ability to code - and the inability to do so, being considered illiteracy. Just as reading and writing once became essential communication skills and with the increasing technologization, it doesn’t appear unlikely the same will be true for coding.
<br>

References:
- https://p5js.org/reference/ 
- Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93

