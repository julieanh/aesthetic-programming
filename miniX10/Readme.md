[Link to miniX](https://julieanh.gitlab.io/aesthetic-programming/miniX7/) <br>

<img src="julieflowchart.png" width=1000> <br>
<img src="feedIt.jpg" width=1000> <br>
<img src="test.jpg" width=1000> <br>

##### What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?
By trying to simplify things to ease communication about the algorithm, miscommunication can arise, and details might be lost or obscured. The actual functionality of the algorithm might be very complex, so in order to communicate it to others, it gets simplified. But that means making choices about what to include and what to exclude, leaving certain aspects of the code in the dark. This might not be communicated either, giving a wrongful impression of the simplified explanation to be the truth of the algorithm by letting it stand alone as the sole representation of the code. <br>
> “To acknowledge that any particular flowchart satisfied no one entirely and was the subject of constant critique, conflict, and negotiation is simply to recognize that, like all maps, the flowchart represented only a selective perspective on reality.” - Ensmenger p. 346

##### What are the technical challenges facing the two ideas and how are you going to address these?
While working on developing ideas for our final miniX, we had some good discussions but found ourselves including a lot of different topics from the curriculum. So, I believe one of the challenges we will face is trying to focus our code and not try to include everything. Another will possibly be to maintain an overview of the project, if we are to work on parts of the code individually or paired up within our group. Here the flowchart will come in handy.
<br>

##### In which ways are the individual and the group flowcharts you produced useful?
For working together as a group, it proved to be a useful tool in creating a shared understanding of the vision for our project ideas. Furthermore, by doing it using an online tool (miroboard) which allowed all members in the group to make suggestions and alterations. After working out an initial flowchart, the visual flow of the idea served as a foundation for new idea generating. For the programming step in the process, it will be easier to divide the different elements of the code amongst each other while maintaining an understanding of the program as a whole. <br>

By going back and make a flowchart over a previous project, I found it was a useful tool to organize the complexity of the code and gain a different perspective. Made me think about the code differently and highlighted the fact that some aspects will be left behind in the process of simplifying it. I made choices as to how much and how I included parts of my code in the flowchart. 

##### References
- Nathan Ensmenger, “The Multiple Meanings of a Flowchart,” Information & Culture: AJournal of History 51, no.3 (2016): 321-351
