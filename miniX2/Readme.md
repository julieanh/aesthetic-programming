# MiniX2
<br>
[Link to Introvert](https://julieanh.gitlab.io/aesthetic-programming/miniX2/introvert/)
<br>
[Link to Gollum](https://julieanh.gitlab.io/aesthetic-programming/miniX2/gollum/)

#### Introvert emoji
I’ve made two quite different emoji-concepts. 
<br>
The first one is an introvert-emoji, made by using different geometric shapes. 
<br>
There is a dark `background` with silhouettes of people and a house with warm bright light on inside with a silhouette of a person sitting by themselves. 
<br>
<br>
<img src ='introvertscreenshot.png' width=400> 

<br>

### Used functions
To make up the house I’ve used `vertex()` to create a custom shape and used `strokeWeight()` to create the illusion of thick walls. For the shape that make up the body of the person I used `bezier()` that creates a custom shape, almost like an ellipse with part of it cut off. I used the same function to create the legs. For the head I used an `ellipse()`. For the people outside of the house, I used a copy of the model used for the person sitting inside the house but slightly altered. To make them appear multiple times I used a `for` loop and a custom function `function makeMan()`, making the shape repeat itself in a horizontal line over the canvas. Within the function I used a varible `let locX = 0` (location X) that I added a value to, making X move slightly for each repetition until `locX < width`. By playing around with adding different values to the shapes repeating I ended up with these different sized shapes almost leaning as to peak in.
<br>
#### Thoughts
Much inspired by my buddy-mate [Christians emoji](https://gitlab.com/ChristianRohde/p5/-/tree/master/miniX2) I thought about creating something that represented some complex emotions and I wanted to create something personal. So, I thought of how to grasp feeling introverted and what that means to me and part of that is feeling more comfortable at home and by myself, illustrated by the person alone in the house. I can find it a bit overwhelming being around other people, making me unsure, illustrated by the dark silhouettes of people. I find that emojis express a certain range of emotions, like happy, sad or angry and then different degrees of such. I thought it was interesting to make something that expressed something more complex. It’s also a different take on the increasing complexity of the emojis, here it’s not really the visual part getting more complex but the emotion the emoji is trying to convey. 
What I didn’t manage to capture in this emoji, is that being an introvert isn’t equal to you not wanting to interact or be around other people. Furthermore, that there can be different degrees of introvert and you’re not necessarily either introvert or its counterpart, extrovert. This is not an uncommon perception and failing to capture this in my emoji, could lead to feeding into a misleading narrative. 
<br>
It’s not a coincidence that I didn’t give any of the people a face. Or any sort of personal identifiers. I wanted to create something as many as possible could identify themselves with no matter your cultural background. Essentially make an objective design. But as there is no such thing as unbiased or neutral design, there is also something to be said for the shape I’ve chosen. It’s part of this widely used figure for person seen on many signs throughout the world. But not just person, I think a vast majority, if asked, would pick the person to be male. Largely based of gendered bathroom signs. 
So, there is still some things I would try to work on, should this be used for a larger userbase.
<br>

## Another attempt or take on an emoji
I have made another emoji of sorts. It is a background consisting of an image of the fictional world of Middle earth and an image of Gollum, known from Lord of the Rings that functions as a play/stop button. I was inspired by the emoji that my buddy-mate Svend made ([click here to see emoji]( https://gitlab.com/svendbay/aesthetic_programming/-/tree/master/miniX2)). 
<br>

<img src ='gollumscreenshot.png' width=300> 
<br>

#### Used functions
I created a canvas and made the background set to a variable `mapRings` which set the background to the preloaded image of Middle earth. By putting media elements in `function preload()` I avoid lag when the page is loading. I place an image of Gollum on top of the background `createImg()` and within  `mouseClicked()` I use `function playHobbits()`. That is a custom function where I’ve used an `if()` statement together with the reference `isPlaying()` and a custom variable for the audio `hobbits`, so that if `hobbits` is playing, then `stop()`. Making the audio stop, when mouse is clicked, if it is playing. `else` the audio `hobbits` will play, meaning that if the audio is not playing and mouse is clicked, the audio will play. 
<br>
#### Thoughts 
The technical visual part of this is emoji is not particularly complex, but I thought about how emojis are used to express yourself online. I thought about how I express myself and came to think about how frequently I make movie quotes, internet video-memes and song references. Which lead me to thinking about incorporating audio in my emoji. The audio played is one of my friends from an online community making an impression of the character, which I enjoyed incorporating as a personal detail. I also thought about instead of making a universal sign for a specific emoji, to make use of characters as we know them. Like how we use memes or gifs. There, the references often appear as they are known. I’ve thought about this since, and this approach is quite faulty since it differs a lot in various cultures what your references are. 

#### References
- [Link to Svends miniX](https://gitlab.com/svendbay/aesthetic_programming/-/tree/master/miniX2)
- https://p5js.org/reference/#/p5/vertex
- Daniel Schiffman: https://www.youtube.com/watch?v=76fiD5DvzeQ&t=128s
- [Christians miniX](https://gitlab.com/ChristianRohde/p5/-/tree/master/miniX2)
