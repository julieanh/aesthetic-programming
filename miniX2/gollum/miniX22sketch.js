//Varibles
let gollum, mapRings;
let gButton;

//Picture and sound is pre-loaded in order to avoid delay
function preload() {

//Pictures
  gollum = loadImage('gollum.png');
  mapRings = loadImage('mapRings.jpg');

//Sound
  hobbits = loadSound("hobbits.mp3");
}


function setup() {
createCanvas(500, 550);
background(mapRings);

//This creates the custom button (here called gButton), here a picture of Gollum
  gButton = createImg('gollum.png');
  gButton.position(80, 80);
  gButton.style("100px");
//This explains an action, that when mouse is clicked refer to the custom function
  gButton.mouseClicked(playHobbits);
}

function playHobbits(){
  if (hobbits.isPlaying()) {
    // .isPlaying() returns a boolean
    hobbits.stop();
  } else {
    hobbits.play();
  }
}
