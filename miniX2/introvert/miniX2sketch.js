let locX = 0;

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(15);
  background(4, 4, 106);
  angleMode(DEGREES);
  }

function draw() {
  console.log(mouseX, mouseY)


//House
stroke(0);
strokeWeight(20);
fill(244, 208, 63);
  beginShape();
    vertex(320, 320);
    vertex(480, 160);
    vertex(640, 320);
    vertex(640, 640);
    vertex(320, 640);
  endShape(CLOSE);


//Body
  stroke(0, 0, 0);
  strokeWeight(2);
  fill(0);
  bezier(360, 630, 360,500, 450,500, 450,630);


//Left leg
  stroke(0, 0, 0);
  strokeWeight(2);
  fill(0);
  bezier(380,630,370,680,345,717,330,700);

//Right leg
  stroke(0, 0, 0);
  strokeWeight(2);
  fill(0);
  bezier(430,630,430,680,423,717,400,690);

//Head
  ellipse(405,505,60);

//People
  stroke(0, 0, 0);
  strokeWeight(2);
  fill(0);
  bezier(360, 630, 360,500, 450,500, 450,630);

//Fordi jeg bruger en custom function så laver vi en global variable i stedet for at lave den i for loopet.
for (/*her ville variable ellers være hvis ikke for global*/;locX < width; locX+=130) {
makeMan();
}

}

function makeMan () {
  //Body
    stroke(0, 0, 0);
    strokeWeight(2);
    fill(0);
    //bezier(360, 630, 360,500, 450,500, 450,630);
    bezier(locX+65+random(+20,-20), 630, locX+65,500, locX,500, locX+random(+20, -20),630);
    //bezier(locX+65, 630, locX+65,500, locX,500, locX,630);
    //Head
      ellipse(locX+32.5,505,60);
        //ellipse(405,505,60);

}
