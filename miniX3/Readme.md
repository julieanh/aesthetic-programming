[See my project here](https://julieanh.gitlab.io/aesthetic-programming/miniX3/) Tip: Try hitting f11 while viewing (just for aesthetics sake)
<br>
Can also try to hold down Ctrl (command on mac I assume) and scroll with mousewheel to zoom in and out.

[See my code here - with comments](https://gitlab.com/julieanh/aesthetic-programming/-/blob/master/miniX3/miniX3sketch.js)
<br>
[See my code here - less comments](https://gitlab.com/julieanh/aesthetic-programming/-/blob/master/miniX3/miniX3sketch-noComments.js)
<br>

## Perserverance
The program is a small part of the solar system. There is planet earth, a moon orbiting the earth, a spaceship and mars orbiting the sun. Throughout the canvas there is stars placed randomly for each time the page is loaded, blinking in slightly different patterns. 
<br>
Initially I wanted to create the solar system with the different planets, but I thought it was quite fitting to tie my project together with the recent landing of Perseverance on Mars by including a little spaceship. As different major buildings have been lit up with red lights to mark the event of landing on ‘the red planet’, I chose that same colour for the spaceship. <br>
I wanted to create a throbber that was more than a small icon and I like how it turned out to be a link to recent events. <br>

<img src ='empire.jpg' width=340> <img src ='snapshot.png' width=400>
<br>
The Empire State Building illuminated for Mars Perseverance

### Syntaxes
For this project I made use of quite a few syntaxes. Fist of, I’ve used `variables` connected to an `array`. I used the `preload` function for the pictures to avoid delay when loading the page. The pictures I made using Paint. I’ve used `while` loops to for example generate the stars. By using `translate` I could manipulate where I wanted the place of origin for different objects. I’ve used a lot of `push` and `pop` to keep track of my code and avoid unwanted interference within my code. I worked with `arc` to create detail in the sun as well as using `bezier` in p5 playground to create the sunbeams. `rotate` was used together with `frameCount` to make things orbit and turn, and continuously do so creating the animated effect. 
I’ve used `angleMode` to change between degrees and radians, in a slightly messy way that I think I will try to study a bit on my code. I also learned that push and pop will not save or isolate angleMode. I’ve come to understand 3 radians count as more than 3 degrees which is quite small, so the values would have to change quite significantly in changing between the two. I’ve also used the `sin` to control the pattern of the shifting size of the stars. This was an idea from an electronics engineer friend of mine who I worked closely together with in the making and understanding of the code. 

In my code I have made quite a lot of comments and explanations about my code. This is both for people wishing to understand what I did, but also for myself to go back a re-study my own code and remember the reasoning behind it for future use. 

A visualisation of how the size of the stars fluxuate in the pattern of a sinus wave. <br>
<img src ='sinus.png' width=300>


### My thoughts
I am very happy with how this project turned out and I’ve spent a lot of time working on it this past week. I have always had trouble with asking for help for various reasons and if there was an option to do something by myself, I would never choose to involve other people. But starting this education, that has changed. For this project I have, aside from using the tools we’ve been given, consulted with my buddy group on different problems, I made use of both instructor class and the shut up and code. I’ve discussed it with my partner and with a friend of mine with coding experience. My friend and I ended up sitting for hours during the weekend and discussing it and he was a great help in making me understand the solution to the things I wanted to include in my design and why things work. I would not have learned as much by being entirely on my own. 

#### Related readings
If I were to tie it together with some of the assigned readings, I come to think of “new way of thinking” in Annette Vee ‘Coding for everyone’. Working together with my friend with coding experience I could definitely sense a difference in the way he approached the design problems by breaking them down. I think this is something I will eventually learn with practice, but it was a great learning experience. 
Thinking of Hans Lammerant ‘The ends of time’ it could be interesting to try and work with linking the rotation to a certain time frame. Or create a day-night-time cycle in the way the design looks depending on sunrise/sundown. 

#### References
- https://www.nasa.gov/image-feature/the-empire-state-building-illuminated-for-mars-perseverance
- Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93.
- Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018), https://monoskop.org/log/?p=20190.

