let starsMinSize = 2;
let starsMaxSize = 6;

let starsStaticX = ["0"];
let starsStaticY = ["0"];
let starsStaticSize = ["0"];
let numStars = 300; //of each type, i.e. 100 of static and blinky

let starsBlinkyX = ["0"];
let starsBlinkyY = ["0"];
let starsBlinkiness = ["0"];

let windowWidthsave;
let windowHeightsave;

//---------------------------------------------------------------PRELOAD--------
function preload(){
  earth = loadImage('earth.png');
  rocket = loadImage('rocket.png');
  mars = loadImage('mars.png');
}
//---------------------------------------------------------------SETUP----------
function setup() {
  createCanvas(windowWidth,windowHeight);
  windowWidthsave = windowWidth;
  windowHeightsave = windowHeight;

  frameRate(60);
  angleMode(DEGREES);


  let i = 0;
  while(i < numStars){

    starsStaticX.push(random(window.screen.width)*2);
    starsStaticY.push(random(window.screen.height)*2);
    starsStaticSize.push(random(starsMinSize,starsMaxSize));

    starsBlinkyX.push(random(window.screen.width)*2);
    starsBlinkyY.push(random(window.screen.height)*2);
    starsBlinkiness.push(random(starsMinSize,starsMaxSize))
   i=i+1;
  }

}

//------------------------------------------------------------DRAW--------------
function draw() {
background(1, 13, 48);

//LOOP START//---------------------------------------------------STARS----------
let x = 0;
while(x <numStars){
  fill('white');
  noStroke();

  ellipse(starsStaticX[x],starsStaticY[x],starsStaticSize[x]);
  ellipse(starsBlinkyX[x],starsBlinkyY[x],starsBlinkiness[x]);

  starsBlinkiness[x] = starsMaxSize*sin(frameCount*(x/100) + x)-2;
  x=x+1;
}

translate(windowWidth/2, windowHeight/2);
//-------------------------------------------------------------------SUN--------
push();
//Outer sun color (orange)
    noStroke();
    fill(243, 156, 18 );
    ellipse(0,0,275.5);
//Sun itself
    stroke(0);
    strokeWeight(5);
    fill(234, 188, 2);
    ellipse(0,0, 200);
pop();

//White detail inside sun
push();
    stroke(255);
    strokeWeight(5);
    noFill();
    arc(0,0, 170, 170, -30, 50, PI + QUARTER_PI);
    arc(0,0, 170, 170, 60, 65, PI + QUARTER_PI);
pop();

//----------------------------------------------------------SUN BEAMS-----------
push();
let angle = 0;
while (angle<360){
  rotate(angle);
  sunBeam()
  angle += 90;
}
pop();

function sunBeam(){
    fill(1, 13, 48);
    stroke(0);
    strokeWeight(5);
    bezier(148,-1,107,-2,120,31,147,36);
    bezier(145,38,102,35,107,60,126,82);
    bezier(126,81,87,66,82,89,85,110);
    bezier(43,135,43,116,54,88,88,109);
    bezier(41,136,30,110,-1,118,-1,146);
}

//----------------------------------------------EARTH/MOON ORBIT CIRCLE---------
push();
    noFill();
    stroke(46, 63, 114);
    strokeWeight(0.8);
    ellipse(0,0,500);
pop();

//-------------------------------------------------------EARTH & MOON-----------
push();
angleMode(RADIANS);

//Earth
  rotate(frameCount/150);
  translate(250, 0);
  imageMode(CENTER);
  image(earth, 0, 0, 50, 50);

//Moon
  push();
  fill(144, 148, 151 );
  rotate(frameCount/150);
  translate(50,50);
  imageMode(CENTER);
  image(moon, 0, 0, 18, 18);
  pop();

pop();

//--------------------------------------------------------ROCKET ORBIT RING-----
push();
    noFill();
    stroke(46, 63, 114);
    //stroke(0);
    strokeWeight(0.8);
    ellipse(0,0,760);
pop();

//----------------------------------------------------------------ROCKET--------
push();

angleMode(RADIANS);
    //Rocket
      rotate(-frameCount/180);
      translate(380, 0);
      imageMode(CENTER);
      image(rocket, 0, 0, 60, 60);
pop();

//------------------------------------------------------------MARS ORBIT RING---
angleMode(DEGREES);

push();
    noFill();
    stroke(46, 63, 114);
    strokeWeight(0.8);
    ellipse(0,0,1030);
pop();

//------------------------------------------------------------------MARS--------
push();

angleMode(RADIANS);
    //Rocket
      rotate(frameCount/300);
      translate(515, 0);
      imageMode(CENTER);
      image(mars, 0, 0, 25, 25);
pop();

angleMode(DEGREES);
}

//-------------------------------------------------------RESIZE WINDOW----------
function windowResized() {
  resizeCanvas(windowWidth,windowHeight);

var i = 0;
  while (i < numStars){

    starsStaticX[i] = starsStaticX[i]*(windowWidth/windowWidthsave);
    starsStaticY[i] = starsStaticY[i]*(windowHeight/windowHeightsave);
    starsBlinkyX[i] = starsBlinkyX[i]*(windowWidth/windowWidthsave);
    starsBlinkyY[i] = starsBlinkyY[i]*(windowHeight/windowHeightsave);
    i = i+1;
  }

  windowWidthsave = windowWidth;
  windowHeightsave = windowHeight;
}
