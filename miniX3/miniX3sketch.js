let starsMinSize = 2;
let starsMaxSize = 6;

/*
The variables are in an array as when the page is loaded a "list"/array of
stars (numStars = 300) are being generated. (300 blinky + 300 static)
Each star has a number that can be called from
the array throughout the code.
*/
let starsStaticX = ["0"];
let starsStaticY = ["0"];
let starsStaticSize = ["0"];
let numStars = 300; //of each type, i.e. 100 of static and blinky

let starsBlinkyX = ["0"];
let starsBlinkyY = ["0"];
let starsBlinkiness = ["0"];

//save Window resizeCanvas
let windowWidthsave;
let windowHeightsave;

//---------------------------------------------------------------PRELOAD--------
function preload(){
  earth = loadImage('earth.png');
  rocket = loadImage('rocket.png');
  mars = loadImage('mars.png');
  moon = loadImage('moon.png');
}
//---------------------------------------------------------------SETUP----------
function setup() {
  createCanvas(windowWidth,windowHeight);
  windowWidthsave = windowWidth;
  windowHeightsave = windowHeight;

  frameRate(60);
  angleMode(DEGREES);

  //create Stars coordinate Arrays x and y
  //uses screen size for coordinate generation so that there are stars everywhere on the screen
  //otherwise when window is resized larger there would be areas with no stars

//LOOP START//
  let i = 0; //This makes it begin with 1 star and then later adding 1 to i, creating the next star
  //The loop runs until it reaches the set amount of stars (in this case 300)
  //When i (representing amount of stars or times loop has run) is greater than 300, then stop
  while(i < numStars){
    //x-coordinate for star
    starsStaticX.push(random(window.screen.width)*2);
    //y-coordinate for star
    starsStaticY.push(random(window.screen.height)*2);
    //Random size of both w and h for the star (ellipse)
    starsStaticSize.push(random(starsMinSize,starsMaxSize));

//Twinkling stars:
    starsBlinkyX.push(random(window.screen.width)*2);
    starsBlinkyY.push(random(window.screen.height)*2);
    //Picks a random number for the size within the set parameter
    starsBlinkiness.push(random(starsMinSize,starsMaxSize))
    //When loop has run once, add 1 (creating the next star) and run the loop again
   i=i+1;
  }
//LOOP END//
}

//------------------------------------------------------------DRAW--------------
function draw() {
background(1, 13, 48);

//LOOP START//---------------------------------------------------STARS----------
let x = 0;
while(x <numStars){
  fill('white');
  noStroke();
  //This uses the numbers generated from the loop in setup for the coordinates to create the star-ellipses
  ellipse(starsStaticX[x],starsStaticY[x],starsStaticSize[x]);
  ellipse(starsBlinkyX[x],starsBlinkyY[x],starsBlinkiness[x]);

/*
The following creates a sinus wave to use as the fluxtuating size of the star-ellipse.
It takes the stars max size (set to 6) and * it with the sinus.
The sinus is made by using the increasing number of the frameCount,
and * with x which is the specific star in question so to say.

The star number (x) is divided by 100 - it controls the blink speed.
The x in x/100 controls the blinky frequency of the sinus wave, the x makes interval.
The higher the number gets * with framecount, the higher the sinus frequency.
so they all have different frequency.

The + x makes the individual brightness(size) start at different points on the sinus wave.

The -2 makes the bounce of the star shifting in size, it shifts the sin wave down the,
y-scale. The absolute value of the sin-wave then has varying(?) height peaks.
The ellipse scale parameter doesn't accept negative numbers
(since negative size doesn't make sense) so it becomes +2.
Writing +2 would instead add 2 to the starsMaxSize.
*/
  starsBlinkiness[x] = starsMaxSize*sin(frameCount*(x/100) + x)-2;
  x=x+1;
}
//LOOP END//

translate(windowWidth/2, windowHeight/2);
//-------------------------------------------------------------------SUN--------
push();
//Outer sun color (orange)
    noStroke();
    fill(243, 156, 18 );
    ellipse(0,0,275.5);
//Sun itself
    stroke(0);
    strokeWeight(5);
    fill(234, 188, 2);
    ellipse(0,0, 200);
pop();

//White detail inside sun
push();
    stroke(255);
    strokeWeight(5);
    noFill();
    arc(0,0, 170, 170, -30, 50, PI + QUARTER_PI);
    arc(0,0, 170, 170, 60, 65, PI + QUARTER_PI);
pop();

//----------------------------------------------------------SUN BEAMS-----------
push();
let angle = 0;
while (angle<360){
  rotate(angle);
  sunBeam()
  angle += 90;
}
pop();

function sunBeam(){
    fill(1, 13, 48);
    stroke(0);
    strokeWeight(5);
    bezier(148,-1,107,-2,120,31,147,36);
    bezier(145,38,102,35,107,60,126,82);
    bezier(126,81,87,66,82,89,85,110);
    bezier(43,135,43,116,54,88,88,109);
    bezier(41,136,30,110,-1,118,-1,146);
}

//----------------------------------------------EARTH/MOON ORBIT CIRCLE---------
push();
    noFill();
    stroke(46, 63, 114);
    strokeWeight(0.8);
    ellipse(0,0,500);
pop();

//-------------------------------------------------------EARTH & MOON-----------
push();
angleMode(RADIANS);

push();
//Earth
  rotate(frameCount/150);
  //Builds on top of the earlier set translate
  translate(250, 0);
  imageMode(CENTER);
  image(earth, 0, 0, 50, 50);

//Moon
  push();
  fill(144, 148, 151 );
  rotate(frameCount/150);
  //Within the same push n pop as the earth so building further
  //on the same translate
  translate(40,40);
  //ellipse(0,0,12,12);
  imageMode(CENTER);
  image(moon, 0, 0, 18, 18);
  pop();

pop();

//--------------------------------------------------------ROCKET ORBIT RING-----
push();
    noFill();
    stroke(46, 63, 114);
    //stroke(0);
    strokeWeight(0.8);
    ellipse(0,0,760);
pop();

//----------------------------------------------------------------ROCKET--------
push();

angleMode(RADIANS);
    //Rocket
      rotate(-frameCount/180);
      translate(380, 0);
      imageMode(CENTER);
      image(rocket, 0, 0, 60, 60);
pop();

//------------------------------------------------------------MARS ORBIT RING---
angleMode(DEGREES);

push();
    noFill();
    stroke(46, 63, 114);
    //stroke(0);
    strokeWeight(0.8);
    ellipse(0,0,1030);
pop();

//------------------------------------------------------------------MARS--------
push();

angleMode(RADIANS);
    //Rocket
      rotate(frameCount/300);
      translate(515, 0);
      imageMode(CENTER);
      image(mars, 0, 0, 28, 28);
pop();

angleMode(DEGREES);
}

//-------------------------------------------------------RESIZE WINDOW----------
function windowResized() {
  resizeCanvas(windowWidth,windowHeight);

//adjusting star coordinates when window resizes
var i = 0;
  while (i < numStars){
    /*
    i is stars and every star is pulled from the same array as earlier.
    When you resize the window it is fixing the coordinates; the stars get adjusted
    to the new window size.
    Both for the static stars x and y and the ones who blink.
    */
    starsStaticX[i] = starsStaticX[i]*(windowWidth/windowWidthsave);
    starsStaticY[i] = starsStaticY[i]*(windowHeight/windowHeightsave);
    starsBlinkyX[i] = starsBlinkyX[i]*(windowWidth/windowWidthsave);
    starsBlinkyY[i] = starsBlinkyY[i]*(windowHeight/windowHeightsave);
    i = i+1;
  }

//update saved window dimensions for use with updating star coordinate
//This saves what the window width was before a change was made
//and based on that it re-adjusts the position of the star in the loop.
  windowWidthsave = windowWidth;
  windowHeightsave = windowHeight;
}
