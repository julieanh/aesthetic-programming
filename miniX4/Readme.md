[See my project here](https://julieanh.gitlab.io/aesthetic-programming/miniX4/) 
<br>

[See my code here](https://gitlab.com/julieanh/aesthetic-programming/-/blob/master/miniX4/miniX4sketch.js) 
<br>

#### What have I made

The program consists of an input box with a headline above it instructing the user to write a word that describes themselves compared to other people. Next to the input box there is a button saying ‘comment’. The background is a video from the user’s camera displaying images in a series appearing as a live feed. When the button is pressed a # + the word typed in input will appear via face tracking on the user’s forehead with a handwritten-style font as if someone scribbled on their face. The headline will also change to “You are ” + # + the input word. In the right bottom corner, red text saying, “Screenshot saved” will appear in a bit-like font. <br>
(A picture has been used for demonstration)

<img src ='face1.png' width=340> <img src ='face2.png' width=340>

##### Syntaxes
I’ve made use of the `createCapture` to include video. The `clm.tracker` creates points for facetracking and `getCurrentPosition` lets me access specific points. `createInput` and `createButton` was used to create an area where input could be typed with a button next to it. `createElement` lets me include html content creating the headline. `filter` makes it possible to apply a video filter, in this project a grayscale filter removing all colours. <br>

### A personal reflection on the rapid capture of images for uploads and comparison
The program captures the data input when the button is pressed, creating a fictitious hashtag that is displayed on the captured video of the user tracking their forehead. A fictional screenshot of the user is captured with the hashtag on their face. The text doesn’t explain what it is saved for, where it is saved or for how long it is saved. I chose the red colour for it’s bold and almost aggressive symbolism since the program does something the user had no knowledge about or ability to accept or decline. What will now happen to the captured image is out of the user’s hands and influence.  And it is not just that it's saving a screenshot that you have no control over, it's also a reflection on how something that happened once, or an impression someone had of you once can linger with you - you might have accepted or agreed with the opinion once, but in this day and age, that picture will live on forever. <br>
My project is in part also the result of a reflection on the different labels we put on ourselves and each other both online and offline. We have cameras in our phones ready at any moment to capture ourselves and each other that we can later retrieve or find online to look at. I drew inspiration from Instagram where pictures - especially selfies - and hashtags are a big part of the app. You label your images, often selfies with a caption that often includes hashtags. You try to find one or few words that capture some element about your picture, to fit what you want to say about it into this label. Another inspiration for this project was the Aarhus University official Instagram account where they let a student ‘take over’ the account creating uploads for a period of time before passing it on to the next student. As a kind of ‘by students, for students’-theme with the content focusing on student life; a look into their chosen education, themselves and their daily life. The uploads on the account collectively creates a visual representation of an Aarhus University student. I reflected on my own experience with this account, comparing myself to what could be interpreted as the standard university student. I found that I in many ways did not fit this image or related to the visual representation of student life. I then reflected upon how Instagram content often depicts a staged reality. What appear as captured snapshots of real life, is staged and carefully curated situations that often later gets edited to create the desired aesthetic. So, what I end up comparing myself to is a fictional reality that likely to some degree is meant as a form of advertisement for the university. <br>

##### References
[p5 input/button example](https://p5js.org/examples/dom-input-and-button.html) <br>
[Face tracking](https://github.com/auduno/clmtrackr)<br>

Fonts used in project: <br>
[distorted](https://www.dafont.com/go-around-the-books.font) <br>
[bit font](https://www.dafont.com/pixeled.font)
<br>
