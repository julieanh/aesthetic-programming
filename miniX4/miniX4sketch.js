let capture;
let ctracker;
let input, button, greeting;
let buttonPressed;
let nameSaved;
let myFont;

function preload() {
disFont = loadFont('distortedfont.ttf');
pixelFont = loadFont('pixelfont.ttf');
}
//------------------------------------------------------------------------------

function setup() {
  createCanvas(640,480);
  background(100);

//-------------------------------------------------------------------------Video
  capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.hide();

//-------------------------------------------------------------------Facetracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

//------------------------------------------------------------------Button input
input = createInput();
  input.position(20, 65);

  button = createButton('comment');
  button.position(input.x + input.width, 65);
  button.mousePressed(greet);


  greeting = createElement('h2', 'Compared to others, I am in one word...');
  greeting.position(20, 5);

  textAlign(CENTER);
  textSize(50);

  function greet() {
    let name = input.value();
    greeting.html('You are ' + '#' + name);
    input.value('');

    buttonPressed = true
    nameSaved = name
}
}

//---------------------------------------------------------------------Setup end

function draw() {

  image(capture, 0,0, 640, 480);

  let positions = ctracker.getCurrentPosition();

//--------------------------------------------------------------Facetracker dots
/*
  if (positions.length) {
    for(let i=0; i< positions.length; i++) {
      noStroke();
      fill(255);
      ellipse(positions[i][0], positions[i][1], 2,2);
    }
  }
*/
//----------------------------------------------------------If button is pressed
if (buttonPressed){
  filter(GRAY);

if (positions.length){
push();
textFont(disFont);
//stroke(0);
fill('darkred');
text('#' + nameSaved, positions[33][0], positions[33][1]);
  fill('red');
  text('#' + nameSaved, positions[33][0] -1, positions[33][1]);
pop();
}
    push();
    textFont(pixelFont);
      //stroke(0);
      fill('red');
      textSize(10);
      text('Screenshot saved', 558, 475);
      noStroke();
      //ellipse(461, 463, 8);
    pop();
}

} //End of draw
