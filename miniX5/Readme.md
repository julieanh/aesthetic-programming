[See my project](https://julieanh.gitlab.io/aesthetic-programming/miniX5/) <br>
[See my code](https://gitlab.com/julieanh/aesthetic-programming/-/blob/master/miniX5/miniX5.js)
<div align="center">

#### Fuzzy worm

<br>
<img src ='fuzzyWorm.png' width=900>
</div>
<br>

Rules:
1. Change the hue of the shape in a defined spectrum
2. Make the radius of shape continuously vary  
3. Draw shape according to the mouse coordinates <br>

The program is an organic shape following the movement of the mouse in a slow pace. It is constantly generating itself with slightly different variables creating an uneven shape with resemblance of a circle, leaving past imprints of itself on the canvas. As it generates the hue will change color in a defined spectrum. When the mouse is pressed the canvas will be cleared by drawing the background again and the beginning of the shape appear at the placement of the mouse, starting over with leaving a trail behind of its former self. <br>

For this week I thought about the presentation in Software Studies given by group 5 about J3RR1. We had a class discussion about feeling empathy for a computer. I thought it was quite an interesting thought and kept it in mind for this week’s miniX assignment. I wanted to work with a more organic impression than I found 10PRINT to be. My code makes use of a “softer” geometry, contrasting the sharp lines and corners of the 10PRINT code. The trail of imprints continuously generating depending on the coordinates of the mouse and the “fuzzy-ness” of the shape, makes it seem like a worm-like creature that is trying to catch up to the mouse. The way in which the color of the shape is changing back and forth between a blue and green hue suits the movement of the shape as opposed to flashing rapidly. The slowed pace of which it moves could almost make you feel bad for moving too fast and “stressing it out”. If the mouse is moved too fast it can’t figure out where to go and doesn’t really move much across the canvas. If the mouse is not moved at all, the shape will generate itself in one place, not knowing where to go. This creates a sort of symbiotic relationship between the mouse and the shape. Or the code and user. The shape/”worm” will not adjust itself entirely to the user but if worked with within its limitations, something beautiful can be made. I thought of this relationship between code and user in relation to 10 PRINT CHR$(205.5+RND(1)); : GOTO 10 that discusses the apprehension towards computer art. With this piece of code, I think (apart from the code) the shape almost works as a medium like paint or pencils but what the artist does with it, is what makes something more than what it would be on its own. It will be unique to the user who wields it and each time the code is run, creating a different outcome each time. I find that this sort of unpredictability within the set boundaries fits well with the idea of it being more organic. <br>

I’ve worked with code from a design piece I found online. I have tinkered around with the code and worked together with a friend of mine to get a better understanding of the “circle” and how it is generated. I have played around with changing different variables in my code and there is a lot of interesting outputs to be found by for example changing the speed of which it follows the mouse, using an ellipse with clean lines or changing the size. I also worked out a sketch where the shape would be drawn according to Perlin Noise, but I liked this version better. 
I thought this week’s assignment would be easier than it turned out to be. I found it hard to align ideas in my head with what I can make with the code. I trust this will improve with practice. I might choose to work more on this code for next week, I found it quite interesting. 

#### References
- [J3RR1](http://digicult.it/design/j3rr1-empathy-with-the-machine-interview-with-none-collective/)
- [10 PRINT CHR$(205.5+RND(1)); : GOTO 10 - Nick Montfort et. al.](https://10print.org/)
- [Original Generative Code](http://www.generative-gestaltung.de/2/sketches/?01_P/P_2_2_3_01)
