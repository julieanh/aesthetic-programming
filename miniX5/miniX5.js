var formDots = 50; //decides the amount of "points" for shape, fun to mess with
var stepSize = 2;

var initRadius = 80; //"size" of the shape
var centerX;
var centerY;
var x = [];
var y = [];

//keeps it within the blue/green colors
var huemax = 210;
var huemin = 110;

//sets the hue, saturation and brightness of the shape
var circlehue = 200;
var circlesat = 80;
var circlebright = 70;

var colordirection = "up";

//-------------------------------------------------------------------------SETUP
function setup() {
  createCanvas(windowWidth, windowHeight);
background(50);
  // init shape
  centerX = width / 2;
  centerY = height / 2;

  var angle = radians(360 / formDots);
  for (var i = 0; i < formDots; i++) {
    x.push(cos(angle * i) * initRadius);
    y.push(sin(angle * i) * initRadius);
  }


  colorMode(HSB);
  strokeWeight(0.25);
  noFill();

}

//--------------------------------------------------------------------------DRAW
function draw() {

  push();
    colorMode(RGB);
    noStroke();
    fill(90);
    textSize(14);
    text('click mouse to refresh', 15,25);
  pop();

  colorMode(HSB);
  stroke(circlehue,circlesat,circlebright);

//------------------------------------------------------------------Hue of shape
//Makes the shape shift hue from one end of set scale and back again

  if(circlehue>huemax){
    colordirection = "down";

  }else if (circlehue<huemin) {
    colordirection = "up";
  }

  if (colordirection == "up"){
    circlehue += 1;
  } else if (colordirection == "down"){
    circlehue -= 1;
  }

//------------------------------------------------------------------------------

console.log(colordirection);

//--------------------------------------------------------Movement towards mouse
// follows the mouse, last value changes the speed
centerX += (mouseX - centerX) * 0.01;
centerY += (mouseY - centerY) * 0.01;


//----------------------------------------------------------"Fuzzyness" of shape

  var angle = radians(360 / formDots);
  var radius = initRadius// * random(0.5, 1);
  //var mod = random(-stepSize, stepSize);
  for (var i = 0; i < formDots; i++) {
    var mod = random(-stepSize, stepSize);
    x[i] = cos(angle * i) * initRadius + mod;
    y[i] = sin(angle * i) * initRadius - mod;
  }

//-------------------------------------------------------------Shape of "circle"

beginShape();
//creates curvy line between dots/coordinates
  // first controlpoint
  curveVertex(x[formDots - 1] + centerX, y[formDots - 1] + centerY);

  for (var i = 0; i < formDots; i++) {
    curveVertex(x[i] + centerX, y[i] + centerY);
  }
  curveVertex(x[0] + centerX, y[0] + centerY);
  // end controlpoint
  curveVertex(x[1] + centerX, y[1] + centerY);
endShape();

//------------------------------------------------------------------------------

} //end of draw

//-------------------------------------------------------------------------Reset
function mousePressed() {
  // init shape on mouse position
colorMode(RGB);
  background(50);

  centerX = mouseX;
  centerY = mouseY;
}
//------------------------------------------------------------------------------

function windowResized() {
  resizeCanvas(windowWidth,windowHeight);
}
