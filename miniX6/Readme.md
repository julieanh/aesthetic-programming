[See my project](https://julieanh.gitlab.io/aesthetic-programming/miniX6/)
<br>
[See my code](https://gitlab.com/julieanh/aesthetic-programming/-/blob/master/miniX6/miniX6.js)

## Flaming art
<div align="center">
<img src ='minix6-1.png' width=800>
</div>

<br>

I have reworked my miniX5. I had a lot of fun making that project and wanted to expand on it. I have added more customizability in the form of five new sliders. It is now possible to change the colour, size and speed of the new “drawing object”. Instead of the fuzzy circle, the user is now drawing with hateful/spiteful comment from an online gaming environment. I have changed the way the program draws, by having the user decide when to by pressing the mouse. The program then chooses a word from an array to draw with and a new when the mouse is released, giving new word for each time mouse is pressed. I'm really quite happy with how it turned out.
<br>

Using programming as a method for design, I find to be comparable to the way different artistic mediums are being used to create not only aesthetically pleasing designs, but for creating conversations and highlighting issues. It can be used to express the programmer and reflect the programmer’s values, the same way a painter can express themselves on a canvas. 
<br>
> "Aesthetic programming in this sense is considered as a practice to build things, and make
worlds, but also produce immanent critique drawing upon computer science, art, and cultural
theory." - - Aesthetic Programming: A Handbook of Software Studies p. 15 

The critical commentary of my project isn’t demonstrated in the code but can be found in its concept. I thought about what I wanted to say with my project, what within digital culture resonated with me and how to think in the lines of aesthetic programming. I came to think of the recent comments by the artist Oh Land about how people talk to her online which led me to think of the frequent unpleasant rhetoric I encounter in my online gaming environment in World of Warcraft and how widely understood (for some accepted) it is that that is part of playing an online game with chat functions. One could say there is a correlation between concept and code in the sense that it randomly picks the hate from an array of insults not specifically tailored to you. In the same sense, that people write in the game chat to people they don't know personally but fling random unrelated insults at them.<br>

![Alt Text](https://media.giphy.com/media/m2G9bXTHRCUJW/giphy.gif)
<iframe><p><a href="https://giphy.com/gifs/cindy-and-cats-more-my-love-hate-relationship-with-the-internet-m2G9bXTHRCUJW">via GIPHY</a></p>

To give more context, in the game there are chat functions that allow for communication between team members. It is not uncommon that a player will use this chat as a way of expressing their anger/frustration in an unsavoury way often aimed at the team or individual players. Some names for it is raging, flaming or being toxic. <br>
 The words/sentences incorporated in my design are real examples taken from those chats collected in the span of a few days. <br>
I think it is interesting to view them out of their context which to me just emphasizes the absurdity of it and makes my project kind of satirical furthermore by using it for drawing. I think there is something powerful in stripping it from its original malice and putting it in a context I control.
There are ways in which you can filter out the toxicity by ticking of some boxes in chat settings. I believe it’s called a maturity filter, which aims is more towards parents wanting to shield their kids from this rhetoric. This is a way through programming there have been made an attempt to affect this particular digital culture. Players also have the option to report other players leaving it up to modifiers (other humans) to decide the next course of action. It can be compared to calling the police and leaving a report that is then later reviewed. Allowing this function through the programming of the game, is an attempt at combatting unwanted behaviour and can affect how we as people interact with each other online. 
<br>

There is much to be said on the topic of online communications in and outside of gaming culture. I think it is worth noting that I have focused on non-verbal communication and that there is another conversation to be had about the response to feminine voices in online game chats. 

<img src ='minix6-3.png' width=500> <img src ='minix6-4.png' width=500>

#### References
- [My miniX5](https://gitlab.com/julieanh/aesthetic-programming/-/tree/master/miniX5)
- [Article about Oh Land](https://underholdning.tv2.dk/2021-03-07-oh-land-bestormet-af-voldsomme-beskeder-efter-x-factor-beslutning)
- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24
- [Giphy](https://giphy.com/) <br>
 -- [Gif inclusion inspired by Svens miniX6 readme](https://gitlab.com/svendbay/aesthetic_programming/-/tree/master/miniX6)

