//sets the hue, saturation and brightness of the shape
var circlehue = 200;
var circlesat = 80;
var circlebright = 70;

let drawingText = ["stupid cunt", "trash", "the fucks wrong with you", "why you imbeciles","bunch of idiots", "noobs", "moron", "clown of a team", "taking less mentally abled people on a day out", "braindead teammates", "u guys are literally garbage"];

//let huemax, huemin;
let rSlider, gSlider, bSlider;

//-------------------------------------------------------------------------SETUP
function setup() {
 createCanvas(displayWidth * pixelDensity(), displayHeight * pixelDensity());
  
background(80);
frameRate(60);

  // init shape
 centerX = width / 2;
 centerY = height / 2;

colorMode(HSB);
  strokeWeight(0.25);
  noFill();

//-----------------------------------------------------------------Color sliders
rSlider = createSlider(0, 255, 160);
 rSlider.position(20, 20);
 gSlider = createSlider(0, 255, 130);
 gSlider.position(20, 50);
 bSlider = createSlider(0, 255, 180);
 bSlider.position(20, 80);

//--------------------------------------------------------------Text size slider
sizeSlider = createSlider(1, 80, 30, 0);
sizeSlider.position(250, 20);

//------------------------------------------------------------------Speed slider
slider = createSlider(0.0002, 0.3, 0.5, 0);
slider.position(250, 50);


//Picks a word from the array
pickedText = random(drawingText)

}

//--------------------------------------------------------------------------DRAW
function draw() {

//--------------------------------------------------------Slider rect background
  push();
  colorMode(RGB);
  fill(50);
  stroke(60);
  strokeWeight(1);
  rect(0, 0, windowWidth, 120);
  pop();

//--------------------------------Color/Sat/Bright of drawing-text set by slider
//Note that rgb might not stand for red green blue (messy code needs cleanup)

stroke(circlehue,circlesat,circlebright);

const r = rSlider.value();
const g = gSlider.value();
const b = bSlider.value();
stroke(r, g, b);

  push();
  fill(60);
  noStroke();
  textSize(14);
  textAlign(RIGHT);
  text("Press C to clear canvas", windowWidth-50, 100);
  pop();

push();
fill(r, g, b);
noStroke();
textSize(16);
textAlign(RIGHT);
text("FLAMING ART", windowWidth-50, 30);
text("These comments were written in World of Warcraft team chat (instance chat)", windowWidth-50, 50);
pop();

  push();

    fill(70);
    noStroke();

    //Colors sliders text
    text("Color", rSlider.x * 2 + rSlider.width, 35);
    text("Saturation", gSlider.x * 2 + gSlider.width, 65);
    text("Brightness", bSlider.x * 2 + bSlider.width, 95);

    //text size text
    text("Text size", sizeSlider.x + sizeSlider.width + 20, sizeSlider.height * 2);

    //Speed slider text
    text("Speed", slider.x + slider.width + 20, slider.height * 4);

    //Color picker slider text
    text("Chosen color", slider.x + slider.width + 20, 95)

  pop();

//-------------------------------------------------------------Color picker rect
push();
  fill(r, g, b);
  noStroke();
  rect(255, 83, slider.width-5, slider.height);
pop();

//--------------------------------------------------------Movement towards mouse
let val = slider.value();
let sizeVal = sizeSlider.value();

centerX += (mouseX - centerX) * val;
centerY += (mouseY - centerY) * val;

//Activity when mouse is pressed (draws with a word/sentence)
push();
if (mouseIsPressed) {

textSize(sizeVal);
textAlign(CENTER);
text(pickedText, centerX, centerY);

}else {
  pickedText = random(drawingText);
}
pop();


//------------------------------------------------------------------------------

} //end of draw

//-------------------------------------------------------------------------Reset

function keyPressed(){
  if (keyCode === 67){
    colorMode(RGB);
    background(80);
  
    centerX = mouseX;
    centerY = mouseY;
    colorMode(HSB);
  }
}
