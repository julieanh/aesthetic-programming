[See my project](https://julieanh.gitlab.io/aesthetic-programming/miniX7/) | Note: I have had trouble with audio in Chrome but worked in Firefox. <br>
>Beware: Project includes atmospheric audio from hospital, that some might find uncomfortable.

[See my code](https://gitlab.com/julieanh/aesthetic-programming/-/blob/master/miniX7/bubbleClickedTimer.js)

## VacciNation

<div align="center">
<img src ='miniX7img1.png' width=400> <img src ='miniX7img2.png' width=400>
</div>

##### The concept
I have made a very simple vaccination-game.  
The program is a small game where the user is encouraged to vaccinate some bubbles with a covid vaccine. A timer in the upper left corner is calculating how much time (displayed as months) has passed since starting the game. This gives the game a slightly competitive element. When all bubbles have been vaccinated the bubbles freeze/stop moving and text will appear saying “You did it, we can now reopen society” and display how many months it took the user to do so. While the user is vaccinating the bubbles, a sound effect will play in the background consisting of sounds from an intensive care unit in a hospital – including sound from a ventilation machine. When the end-screen is displayed when all bubbles have been vaccinated, the audio changes to a sort of soundscape of city sounds. There is birdsong, traffic noise, people talking.  The two types of audio give the game another dimension and the contrast of the two types of sound is an emphasizing effect of the seriousness of the virus. I think the hospital sounds, is contrasting with the whole simple cartoonish aesthetic of the game, making it slightly ominous by hinting to the potential consequences of infection.

##### Objects
I found object-oriented programming kind of complex even though I can sort of grasp its potential. I used a Daniel Schiffman video and code, that helped me to get a better understanding of creating a class. I also had help from my buddygroup mate [Jonas](https://gitlab.com/jonhegras). The objects in my code is the bubbles which essence is made up by four different functions defined in the Bubble class. The objects are generated throughout the canvas and movies around slightly. An event if clicked on is tied to the objects and is calculated by the distance between coordinates of the mouse and the radius of the object. When it is estimated that the mouse was clicked on the object its brightness changes.

##### Thoughts
My game is quite current in its concept surrounding the corona-virus but that also gives it a sort of temporality. Some might find it politically/ethically provocative since you can only "win" the game, when all have been vaccinated. <br>
I chose the blue color-scheme since blue has been used for official information about corona. <br>
Since finishing the game, I’ve reflected upon the audio implemented in my project of the hospital sounds. While I think it creates and interesting effect and adds dimension, I think for some people it can potentially be quite traumatizing if you’ve recently had a loved one (or yourself) who have been or are hospitalized. I would have liked to implement a way to mute the audio in the game without the user needing to turn off sound on their device, so the end-audio could still be played. 
<br>

There are more things I would have liked to change and worked more with the code but I think it turned out ok.

#### References
- Daniel Schiffman on classes: [https://www.youtube.com/watch?v=T-HGdc8L-7w](https://www.youtube.com/watch?v=T-HGdc8L-7w)
- Daniel Schiffman on mouse-interaction: [https://www.youtube.com/watch?v=TaN5At5RWH8](https://www.youtube.com/watch?v=TaN5At5RWH8)
- [Graphic corona information](https://www.sst.dk/-/media/Udgivelser/2020/Corona/Plakat-beskyt-dig-selv-og-andre/Dansk/plakat_forebyg_smitte_A4_tilgaengelig_dk.ashx?la=da&hash=442D6E47417D15A360A53FCC51F3DDDD8A2D41BC)
- Font found on: [www.dafont.com](https://www.dafont.com/leaves-and-ground.font)
- Audio found on: [www.mixkit.com](https://mixkit.co/free-sound-effects/crowd/)
- Code for timer: [https://stackoverflow.com/questions/50337784/timed-counter-in-p5-js](https://stackoverflow.com/questions/50337784/timed-counter-in-p5-js)



