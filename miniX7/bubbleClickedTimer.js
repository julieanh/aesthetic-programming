let lastPrint;
let i = 0;
let endTime = 5;

let bubbles = [];
let bubbleClick = 0
let img;

let myFont;

let canvasWidth = 1000
let canvasHeight = 800

function preload() {

  myFont = loadFont('leavesFont.ttf');
  drawSyringe = loadImage('drawRotated.png');

  ambiance = loadSound("ambiance.mp3");
  hospital = loadSound("hospital.mp3");

} //end of preload

function setup() {

  createCanvas(canvasWidth, canvasHeight);

hospital.setVolume(0.4);
hospital.play();

//minus 1000 makes it begin at 1 and not 0
lastPrint = millis() - 1000;

  for (let i = 0; i < 34; i++) {
    let x = random(40, 960);
    let y = random(40, 760);
    let r = random(20, 60);
    let b = new Bubble(x, y, r);
    bubbles.push(b);
  }
} //end of setup

//specified in class
function mousePressed() {
  for (let i = 0; i < bubbles.length; i++) {
    bubbles[i].clicked(mouseX, mouseY);
  }
}


function draw() {

  print(bubbleClick);
  background(133, 193, 233);


  //textAlign(RIGHT);
  push();
  textSize(20);
  fill(255);
  stroke(255);
  strokeWeight(1);
  text("How fast can you vaccinate the bubbles?", 20, 40);
  text("Months elapsed:", 20, 65);
pop();
//-------------------------------------------------------------------------TIMER
  var timeElapsed = millis() - lastPrint;
  //console.log(timeElapsed);

  if (timeElapsed > 1000) {
    i++;
    console.log(i);
    lastPrint = millis();
/*
      if (i == endTime){
        background('blue');
        fill(255);
        ellipse(100,100, 40);
        noLoop();
    }
    */
  }
  showTextTime();

  //--------------------------------------time stuff end


  for (let i = 0; i < bubbles.length; i++) {
    bubbles[i].move();
    bubbles[i].show();
  }

//If all the bubbles have been clicked then do...
  if (bubbleClick === bubbles.length) {
    hospital.stop();
    ambiance.setVolume(0.6);
    ambiance.play();

    rect(0,0, canvasWidth, canvasHeight);

push();
    textAlign(CENTER);
    textSize(80);
    fill(98, 156, 195 );
    stroke(255);
    strokeWeight(2);
    textFont(myFont);
    text("< You Did It! >", canvasWidth/2, canvasHeight/2);
    textSize(60);
    text("We can now reopen society", canvasWidth/2, canvasHeight/2 + 50);

    textSize(50);
      stroke(255);
      text("It took you: " + i + " months", canvasWidth/2, canvasHeight/2 + 100);
pop();

    hide(drawSyringe);
    noLoop();
  }

image(drawSyringe, mouseX-305, mouseY-88);

} //end of draw

//--------------------------------------------------------------------TIMER TEXT
function showTextTime(){
  textSize(20);
    stroke(255);
    strokeWeight(2);
    fill(98, 156, 195);
    text(i, 180, 65);
}

//------------------------------------------------------------------Bubble Class
class Bubble {
//skeleton
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.brightness = 0;
  }

//estimating distance, if clicked in object
  clicked(px, py) {
    let d = dist(px, py, this.x, this.y);
    if (d < this.r) {
      if (this.brightness === 0) {
      bubbleClick++
      this.brightness = 255;
    }
    }
  }

//makes the bubbles move
  move() {
    this.x = this.x + random(-2, 2);
    this.y = this.y + random(-2, 2);
  }

//bubble "aesthetic"
  show() {
    stroke(255);
    strokeWeight(4);
    fill(this.brightness, 125);
    ellipse(this.x, this.y, this.r * 2);
  }
} //end of class
