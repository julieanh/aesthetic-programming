let buttonTrue;
let fade = 0
let questionMax;
let selectText;
let curPressed; //used to check if the button is currently being pressed, to ensure

let start = 0
let queNum = 0


function preload(){
  questions = loadJSON("questions.json");

  startButton = loadImage('startButton.png');
  staButPre = loadImage('startButtonPressed.png');

  falseButton = loadImage('falseButton.png');
  trueButton = loadImage('trueButton.png');
  falButPre = loadImage('falseButtonPressed.png');
  truButPre = loadImage('trueButtonPressed.png');

  vine1 = loadImage('vine0nobg.png');
  vine2 = loadImage('vine1nobg.png');
  vine3 = loadImage('vine2nobg.png');
  vine4 = loadImage('vine3nobg.png');

  vine1grey = loadImage('vine0grey.png');
  vine2grey = loadImage('vine1grey.png');
  vine3grey = loadImage('vine2grey.png');
  vine4grey = loadImage('vine3grey.png');

  customFont = loadFont('ocraextended.ttf');
  //customFont = loadFont('OCRAEXT.ttf');
  customFont2 = loadFont('Nagitha.ttf');


}

function setup(){

  startButton = createImg('startButton.png');
  startButton.position(562.5, 550);
  startButton.mouseClicked(startQuestions);

  trueButton = createImg('trueButton.png');
  trueButton.position(312.5, 550);
  trueButton.mousePressed(pressed);
  trueButton.mouseReleased(releaseBut);


  falseButton = createImg("falseButton.png");
  falseButton.position(812.5, 550);
  falseButton.mouseClicked(pressed);
  falseButton.mouseReleased(releaseBut);

  createCanvas(1500, 800);
  background(102, 102, 102);

  textAlign(CENTER);
  textFont(customFont);
  stroke(195);
  strokeWeight(5);
  textSize(40);


} //end of setup


function draw() {
    image(vine1grey, -20, 190);
    image(vine2grey, 900, 0, 505, 226);
    image(vine3grey, 1245, 565, 273, 246);
    image(vine3grey, 100, 0, 425, 186);

  push();
    tint(255, fade - 204)
    image(vine1, -20, 190);
  pop();
  push();
    tint(255, fade - 255)
    image(vine2, 900, 0, 505, 226);
  pop();
  push();
    tint(255, fade - 229.5)
    image(vine3, 1245, 565, 273, 246);
  pop();
  push();
    tint(255, fade - 331.5)
    image(vine4, 100, 0, 425, 186);
  pop();


  if(start === 0){
    trueButton.hide();
    falseButton.hide();
    startButton.show();
    image(staButPre, 562.5, 550);
    fill(102, 102, 102);
    text("Please patiently answer the following statements\nby choosing either 'true' or 'false'", 750, 300)
  } else if(start === 1){
    startButton.hide();
    image(truButPre, 312.5, 550);
    image(falButPre, 812.5, 550);
  }

  if(start === 2){
    trueButton.hide();
    falseButton.hide();
    background(102, 102, 102);
    text("!!!! thanks for your help i found myself\nbest regards\nxoxo", 750, 300)
    push();
      fill(38, 180, 82)
      stroke(8, 90, 33)
      textFont(customFont2);
      textSize(90);
      text("J00-lean dot  file", 750, 500)
    pop();
    image(vine1, -20, 190);
    image(vine2, 900, 0, 505, 226);
    image(vine3, 1245, 565, 273, 246);
    image(vine4, 100, 0, 425, 186);
  }



} //end of draw


function startQuestions() {
  start = 1;

  background(102, 102, 102);
  trueButton.show();
  falseButton.show();
  ask();
}


function pressed() {
//start = 1;
fade += 12.75
if(start === 1){
  if(curPressed === 0){
    this.hide();
    queNum++
    ask();
    curPressed = 1
    }
  }
}

function releaseBut() {
trueButton.show();
falseButton.show();
curPressed = 0
}

function ask() {
  if(start === 1){
    if(queNum < 20){
      for(let i = 0; i < questions.booleans.length;i++){
        questionMax = i
        selectText = floor(random(0, questionMax+1));
        background(102, 102, 102);
        fill(102, 102, 102);
        text(questions.booleans[selectText], 750, 300);
        setTimeout(releaseBut, 200);
    }
  } else if (queNum >= 20){
      for(let i = 0; i < questions.booleansTwo.length;i++){
        questionMax = i
        selectText = floor(random(0, questionMax+1));
        background(102, 102, 102);
        fill(102, 102, 102);
        text(questions.booleansTwo[selectText], 750, 300);
        print(queNum);
        setTimeout(releaseBut, 200);
        if(queNum > 40){
          start = 2;
        }
    }
  }
} else if(start === 2){
  trueButton.hide();
  falseButton.hide();
}

}
